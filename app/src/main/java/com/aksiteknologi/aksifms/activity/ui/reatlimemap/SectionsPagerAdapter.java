package com.aksiteknologi.aksifms.activity.ui.reatlimemap;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.model.Vehicle;
import com.aksiteknologi.aksifms.model.VehicleGroup;

import java.util.ArrayList;


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.realtime_map_first_tab, R.string.realtime_map_second_tab};
    private final Context mContext;
    private final ArrayList<Vehicle> vehicles;
    private final ArrayList<VehicleGroup> vehicleGroups;

    public SectionsPagerAdapter(Context context, FragmentManager fm, ArrayList<Vehicle> vehicles, ArrayList<VehicleGroup> vehicleGroups) {
        super(fm);
        mContext = context;
        this.vehicles = vehicles;
        this.vehicleGroups = vehicleGroups;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        // Log.i(SectionsPagerAdapter.class.getSimpleName(), "-->: "+String.valueOf(position));

        if (position == 0 ) {
            return RealtimeCarPlateFragment.newInstance(vehicles);
        } else {
            return RealtimeCarGroupFragment.newInstance(vehicleGroups);
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return TAB_TITLES.length;
    }
}