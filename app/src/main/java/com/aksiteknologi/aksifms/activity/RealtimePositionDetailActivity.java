package com.aksiteknologi.aksifms.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.model.RealtimePosition;
import com.aksiteknologi.aksifms.model.RealtimePositionDetail;
import com.aksiteknologi.aksifms.model.Vehicle;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class RealtimePositionDetailActivity extends AppCompatActivity {

    private OkHttpClient okHttpClient;
    private SharedPreferences preferences;
    private RealtimePosition realtimePosition;
    private TextView licenseNo, cityLocation, speedIndicator, accIndicator, addressLocation, remark, updateAt;
    private TextView odometer, gsmSignal, latitude, longitude, driver, reportStatus, tempOne, tempTwo;
    private Button showMap;
    private Handler handler = new Handler();
    private Runnable runnable;
    private Switch aSwitch;
    private ImageView carIcon, remarkIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime_position_detail);

        realtimePosition = (RealtimePosition) getIntent().getExtras().getSerializable("detail_data");
        preferences = getApplicationContext().getSharedPreferences(getString(R.string.app_pref_key), MODE_PRIVATE);
        okHttpClient = new OkHttpClient();
        setTitle("Realtime Position Detail");
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    void refreshData() {
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(runnable, 10000);
                try {
                    getVehicleDetail();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, 10000);
    }

    private void initView() {
        aSwitch = findViewById(R.id.auto_refresh);
        licenseNo = findViewById(R.id.license_no);
        cityLocation = findViewById(R.id.city_location);
        speedIndicator = findViewById(R.id.speed_indicator);
        accIndicator = findViewById(R.id.acc_indicator);
        addressLocation = findViewById(R.id.address_position);
        remark = findViewById(R.id.item_remark);
        updateAt = findViewById(R.id.updated_at);

        odometer = findViewById(R.id.odometerValue);
        gsmSignal = findViewById(R.id.gsmValue);
        latitude = findViewById(R.id.latitudeValue);
        longitude = findViewById(R.id.longitudeValue);
        driver = findViewById(R.id.driverValue);
        reportStatus = findViewById(R.id.reportValue);
        showMap = findViewById(R.id.showMap);

        carIcon = findViewById(R.id.icon);
        remarkIcon = findViewById(R.id.remark_icon);

        showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mapView = new Intent(RealtimePositionDetailActivity.this, MapsActivity.class);
                mapView.putExtra("vehicle_data", realtimePosition);
                startActivity(mapView);
            }
        });

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.i(RealtimePositionDetailActivity.class.getSimpleName(), String.valueOf(b));
                if (!b) {
                    handler.removeCallbacks(runnable);
                } else {
                    refreshData();
                }
            }
        });
    }

    private void bindData() {
        licenseNo.setText(realtimePosition.getLicenseNo());
        speedIndicator.setText(realtimePosition.getSpeedIndicator() + " Km/h");
        accIndicator.setText("Acc: "+realtimePosition.getAccIndicator());
        addressLocation.setText(realtimePosition.getAddressPosition());
        remark.setText(realtimePosition.getRemark());
        updateAt.setText(realtimePosition.getGpsTime());

        odometer.setText(String.valueOf(realtimePosition.getDetail().getOdometer()));
        gsmSignal.setText(String.valueOf(realtimePosition.getDetail().getGpsSignal()));
        latitude.setText(String.valueOf(realtimePosition.getDetail().getLatitude()));
        longitude.setText(String.valueOf(realtimePosition.getDetail().getLongitude()));
        driver.setText(realtimePosition.getDetail().getDriver());
        reportStatus.setText(realtimePosition.getDetail().getReportStatus());

        carIcon.setImageDrawable(getDrawable(realtimePosition.getCarIcon()));
        remarkIcon.setImageDrawable(getDrawable(realtimePosition.getRemarkIcon()));
    }

    void getVehicleDetail() throws JSONException {
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");
        String token = preferences.getString("token", "");
        JSONObject param = new JSONObject();
        final JSONArray vehicle = new JSONArray();
        vehicle.put(realtimePosition.getLicenseNo());
        param.put("token", token);
        param.put("items", vehicle);
        param.put("activity", JSONObject.NULL);

        RequestBody requestBody = RequestBody.create(param.toString(),JSON);
        Request request = new Request.Builder()
                .url("http://fms.aksiteknologi.com/index.php/Api/Position/recent")
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    JSONObject vehicleData = new JSONObject(responseBody.string());
                    if (vehicleData.getInt("found") > 0) {
                        JSONArray data = vehicleData.getJSONArray("data");
                        SimpleDateFormat simpleDateFormat;

                        for (int i = 0; i < data.length(); i++) {
                            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            JSONObject item = data.getJSONObject(i);
                            realtimePosition.setCityLocation("");
                            realtimePosition.setSpeedIndicator(String.valueOf(item.getInt("speed")));
                            realtimePosition.setAccIndicator((item.getInt("acc") == 0) ? "Off" : "On");
                            realtimePosition.setAddressPosition(item.getString("address"));
                            realtimePosition.setRemark(item.getString("status_remark") +" "+ item.getString("duration"));
                            Date gpsTimeInDate = simpleDateFormat.parse(item.getString("gps_time"));
                            simpleDateFormat.applyPattern("HH:mm dd MMM yyyy");
                            String gpsTime = simpleDateFormat.format(gpsTimeInDate);
                            // position.setGpsTime(item.getString("gps_time"));
                            realtimePosition.setGpsTime(gpsTime);

                            RealtimePositionDetail realtimePositionDetail = realtimePosition.getDetail();
                            realtimePositionDetail.setOdometer(item.getDouble("odometer"));
                            realtimePositionDetail.setGpsSignal(item.getInt("gsm_signal"));
                            realtimePositionDetail.setLatitude(item.getDouble("latitude"));
                            realtimePositionDetail.setLongitude(item.getDouble("longitude"));
                            realtimePositionDetail.setDriver(item.getString("driver_nm"));
                            realtimePositionDetail.setReportStatus(item.getString("report_nm"));
                            realtimePosition.setDetail(realtimePositionDetail);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bindData();
                            }
                        });
                    }

                } catch (JSONException | ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
