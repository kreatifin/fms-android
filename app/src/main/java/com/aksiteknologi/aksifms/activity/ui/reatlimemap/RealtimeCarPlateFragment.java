package com.aksiteknologi.aksifms.activity.ui.reatlimemap;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.activity.MapsActivity;
import com.aksiteknologi.aksifms.adapter.CarPlateViewAdapter;
import com.aksiteknologi.aksifms.model.Vehicle;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RealtimeCarPlateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RealtimeCarPlateFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private ArrayList<Vehicle> vehicles;

    public RealtimeCarPlateFragment() {
        // Required empty public constructor
    }

    public static RealtimeCarPlateFragment newInstance(ArrayList<Vehicle> vehicles) {
        RealtimeCarPlateFragment fragment = new RealtimeCarPlateFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, vehicles);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            vehicles = getArguments().getParcelableArrayList(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_realtime_car_plate, container, false);
        RecyclerView mainView = view.findViewById(R.id.listView);
        AppCompatButton mapButton = view.findViewById(R.id.showMap);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mainView.setLayoutManager(layoutManager);
        final CarPlateViewAdapter carPlateViewAdapter = new CarPlateViewAdapter(getContext(), this.vehicles);
        mainView.setAdapter(carPlateViewAdapter);

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Vehicle> vehicles = carPlateViewAdapter.getVehicles();
                ArrayList<Vehicle> selectedVehicles = new ArrayList<>();

                for (Vehicle vehicle : vehicles) {
                    if (vehicle.getSelected()) {
                        selectedVehicles.add(vehicle);
                    }
                }
                Intent mapsView = new Intent(getContext(), MapsActivity.class);
                mapsView.putParcelableArrayListExtra("selected_vehicle", selectedVehicles);
                getContext().startActivity(mapsView);
            }
        });

        return view;
    }
}
