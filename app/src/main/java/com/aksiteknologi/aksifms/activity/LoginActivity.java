package com.aksiteknologi.aksifms.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aksiteknologi.aksifms.MainActivity;
import com.aksiteknologi.aksifms.R;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class LoginActivity extends AppCompatActivity {

    private OkHttpClient okHttpClient;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private EditText usernameField, passwordField;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferences = getApplicationContext().getSharedPreferences(getString(R.string.app_pref_key), MODE_PRIVATE);
        okHttpClient = new OkHttpClient();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (preferences.contains("token")) {
            Intent dashboard = new Intent(LoginActivity.this, MainActivity.class);
            dashboard.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(dashboard);

            return;
        }
        initView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length == 1 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Location Permission is needed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void initView() {
        usernameField = findViewById(R.id.usernameField);
        passwordField = findViewById(R.id.passwordField);
        loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usernameField.getText().toString().isEmpty() && passwordField.getText().toString().isEmpty()) {
                    return;
                }

                try {
                    doLogin();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void doLogin() throws JSONException {
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");

        JSONObject param = new JSONObject();
        param.put("email", usernameField.getText().toString());
        param.put("password", passwordField.getText().toString());

        RequestBody requestBody = RequestBody.create(param.toString(),JSON);
        Request request = new Request.Builder()
                .url("http://fms.aksiteknologi.com/index.php/Api/Users/login")
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    JSONObject data = new JSONObject(responseBody.string());
                    if (data.has("status") && data.getString("status").equals("warning")) {
                        final String message = data.getString("msg");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    } else if (data.has("status") && data.getString("status").equals("success")) {
                        Log.i(LoginActivity.class.getSimpleName(), data.getString("msg"));
                        editor = preferences.edit();
                        editor.putString("token", data.getString("msg"));
                        editor.apply();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent dashboard = new Intent(LoginActivity.this, MainActivity.class);
                            dashboard.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(dashboard);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
