package com.aksiteknologi.aksifms.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.model.Vehicle;
import com.aksiteknologi.aksifms.model.VehicleGroup;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.aksiteknologi.aksifms.activity.ui.reatlimemap.SectionsPagerAdapter;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class RealtimeMapActivity extends AppCompatActivity {

    private OkHttpClient okHttpClient;
    private SharedPreferences preferences;
    private ArrayList<Vehicle> vehicles = new ArrayList<>();
    private ArrayList<VehicleGroup> vehicleGroups = new ArrayList<>();
    private KProgressHUD progressHUD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime_map);

        okHttpClient = new OkHttpClient();
        preferences = getApplicationContext().getSharedPreferences(getString(R.string.app_pref_key), MODE_PRIVATE);
        progressHUD = KProgressHUD.create(RealtimeMapActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Getting Data")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (vehicles.size() == 0 && vehicleGroups.size() == 0) {
            try {
                getVehicleList();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void createUI() {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(), vehicles, vehicleGroups);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    void getVehicleList() throws JSONException {
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");
        String token = preferences.getString("token", "");
        JSONObject parameter = new JSONObject();
        parameter.put("token", token);
        parameter.put("plate_no", JSONObject.NULL);
        parameter.put("activity", JSONObject.NULL);

        RequestBody requestBody = RequestBody.create(parameter.toString(),JSON);
        Request request = new Request.Builder()
                .url("http://fms.aksiteknologi.com/index.php/Api/Vehicles/list")
                .post(requestBody)
                .build();

        progressHUD.show();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    JSONObject data = new JSONObject(responseBody.string());
                    JSONArray vehicleData = data.getJSONArray("data");

                    Log.i(RealtimeMapActivity.class.getSimpleName(), data.toString());

                    for (int i = 0; i < vehicleData.length(); i++) {
                        JSONObject vehicle = vehicleData.getJSONObject(i);
                        Vehicle vehicleItem = new Vehicle(vehicle.getString("id"), vehicle.getString("plate_no"), "", "", "", "", false);
                        vehicles.add(vehicleItem);
                    }

                    getVehicleGroup();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void getVehicleGroup() throws JSONException {
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");
        String token = preferences.getString("token", "");
        JSONObject parameter = new JSONObject();
        parameter.put("token", token);
        parameter.put("plate_no", JSONObject.NULL);
        parameter.put("activity", JSONObject.NULL);

        RequestBody requestBody = RequestBody.create(parameter.toString(),JSON);
        Request request = new Request.Builder()
                .url("http://fms.aksiteknologi.com/index.php/Api/Categories/data")
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    JSONObject data = new JSONObject(responseBody.string());
                    JSONArray vehicleData = data.getJSONArray("data");

                    for (int i = 0; i < vehicleData.length(); i++) {
                        VehicleGroup vehicleItem = new VehicleGroup(vehicleData.getString(i), false);
                        vehicleGroups.add(vehicleItem);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressHUD.dismiss();
                            createUI();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}