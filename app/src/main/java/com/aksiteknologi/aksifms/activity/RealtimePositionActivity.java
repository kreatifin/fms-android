package com.aksiteknologi.aksifms.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Switch;
import android.widget.Toast;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.adapter.RealtimePositionViewAdapter;
import com.aksiteknologi.aksifms.model.RealtimePosition;
import com.aksiteknologi.aksifms.model.RealtimePositionDetail;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class RealtimePositionActivity extends AppCompatActivity {

    private RecyclerView mainView;
    private OkHttpClient okHttpClient;
    private SharedPreferences preferences;
    private JSONObject realtimePosData;
    private ArrayList<RealtimePosition> realtimePositions = new ArrayList<>();
    private RealtimePositionViewAdapter realtimePositionViewAdapter;
    private KProgressHUD progressHUD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime_position);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setTitle("Realtime Position");

        preferences = getApplicationContext().getSharedPreferences(getString(R.string.app_pref_key), MODE_PRIVATE);
        okHttpClient = new OkHttpClient();
        progressHUD = KProgressHUD.create(RealtimePositionActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Getting Data")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        initView();

    }

    private void initView() {
        mainView = findViewById(R.id.main_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mainView.setLayoutManager(layoutManager);
        realtimePositionViewAdapter = new RealtimePositionViewAdapter(this, this.realtimePositions);
        mainView.setAdapter(realtimePositionViewAdapter);
    }

    private void getRealtimePositionData() throws JSONException {
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");
        String token = preferences.getString("token", "");
        JSONObject parameter = new JSONObject();
        parameter.put("token", token);
        parameter.put("items", JSONObject.NULL);
        parameter.put("activity", JSONObject.NULL);
        Log.i(RealtimePositionActivity.class.getSimpleName(), token);
        Log.i(RealtimePositionActivity.class.getSimpleName(), parameter.toString());

        RequestBody requestBody = RequestBody.create(parameter.toString(),JSON);
        Request request = new Request.Builder()
                .url("http://fms.aksiteknologi.com/index.php/Api/Position/recent")
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    realtimePosData = new JSONObject(responseBody.string());
                    Log.i(RealtimePositionActivity.class.getSimpleName(), realtimePosData.toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processData();
                            } catch (JSONException | ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @SuppressLint("SimpleDateFormat")
    private void processData() throws JSONException, ParseException {
        progressHUD.dismiss();
        SimpleDateFormat simpleDateFormat;
        JSONArray data = this.realtimePosData.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            JSONObject item = data.getJSONObject(i);

            RealtimePosition position = new RealtimePosition();
            position.setLicenseNo(item.getString("plate_no"));
            position.setCityLocation(item.getString("provinsi"));
            position.setSpeedIndicator(String.valueOf(item.getInt("speed")));
            position.setAccIndicator((item.getInt("acc") == 0) ? "Off" : "On");
            position.setAddressPosition(item.getString("address"));
            position.setRemark(item.getString("status_remark") +" "+ item.getString("duration"));
            Date gpsTimeInDate = simpleDateFormat.parse(item.getString("gps_time"));
            simpleDateFormat.applyPattern("HH:mm dd MMM yyyy");
            String gpsTime = simpleDateFormat.format(gpsTimeInDate);
            // position.setGpsTime(item.getString("gps_time"));
            position.setGpsTime(gpsTime);

            RealtimePositionDetail detail = new RealtimePositionDetail();
            detail.setOdometer(item.getDouble("odometer"));
            detail.setGpsSignal(item.getInt("gsm_signal"));
            detail.setLatitude(item.getDouble("latitude"));
            detail.setLongitude(item.getDouble("longitude"));
            detail.setDriver(item.getString("driver_nm"));
            detail.setReportStatus(item.getString("report_nm"));
            position.setDetail(detail);

            if (item.getString("status_remark").toLowerCase().contains("driving")) {
                position.setCarIcon(R.drawable.ic_car_green);
                position.setRemarkIcon(R.drawable.ic_drive_status);
            } else if (item.getString("status_remark").toLowerCase().contains("parking")) {
                position.setCarIcon(R.drawable.ic_car_blue);
                position.setRemarkIcon(R.drawable.ic_parking_status);
            }

            this.realtimePositions.add(position);
        }

        realtimePositionViewAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (realtimePositions.size() == 0) {
            progressHUD.show();
            try {
                getRealtimePositionData();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
