package com.aksiteknologi.aksifms.activity;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.adapter.MapsInfoWindowAdapter;
import com.aksiteknologi.aksifms.model.RealtimePosition;
import com.aksiteknologi.aksifms.model.RealtimePositionDetail;
import com.aksiteknologi.aksifms.model.Vehicle;
import com.aksiteknologi.aksifms.model.VehicleGroup;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private OkHttpClient okHttpClient;
    private SharedPreferences preferences;
    private ArrayList<Vehicle> vehicles;
    private ArrayList<VehicleGroup> vehicleGroups;
    private ArrayList<RealtimePosition> realtimePositions = new ArrayList<>();
    private RealtimePosition realtimePosition;
    Handler handler = new Handler();
    Runnable runnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        preferences = getApplicationContext().getSharedPreferences(getString(R.string.app_pref_key), MODE_PRIVATE);
        okHttpClient = new OkHttpClient();

        vehicles = getIntent().getParcelableArrayListExtra("selected_vehicle");
        vehicleGroups = getIntent().getParcelableArrayListExtra("selected_group");
        realtimePosition = (RealtimePosition) getIntent().getExtras().getSerializable("vehicle_data");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (vehicles != null) {
            Log.i(MapsActivity.class.getSimpleName(), "Get Vehicle...");
            try {
                getVehicle(false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (vehicleGroups != null) {
            try {
                getVehicleGroup(false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (realtimePosition != null) {
            mMap.setInfoWindowAdapter(new MapsInfoWindowAdapter(MapsActivity.this, realtimePosition));
            LatLng car = new LatLng(realtimePosition.getDetail().getLatitude(), realtimePosition.getDetail().getLongitude());
            MarkerOptions truck = new MarkerOptions().position(car).title(realtimePosition.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_yellow));
            if (realtimePosition.getRemark().toLowerCase().contains("driving")) {
                truck = new MarkerOptions().position(car).title(realtimePosition.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_green));
            } else if (realtimePosition.getRemark().toLowerCase().contains("parking")) {
                truck = new MarkerOptions().position(car).title(realtimePosition.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_blue));
            }

            // BitmapDescriptorFactory.fromResource(R.drawable.ic_truck_blue)
            mMap.addMarker(truck);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(car, 15.0f));
            //mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (vehicles != null) {
            handler.postDelayed(runnable = new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(runnable, 10000);
                    try {
                        getVehicle(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, 10000);
        }

        if (vehicleGroups != null) {
            handler.postDelayed(runnable = new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(runnable, 10000);
                    try {
                        getVehicleGroup(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, 10000);
        }
    }

    private void addToMap() {
        for (RealtimePosition position : realtimePositions) {
            LatLng car = new LatLng(position.getDetail().getLatitude(), position.getDetail().getLongitude());
            MarkerOptions truck = new MarkerOptions().position(car).title(position.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_yellow));
            if (position.getRemark().toLowerCase().contains("driving")) {
                truck = new MarkerOptions().position(car).title(position.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_green));
            } else if (position.getRemark().toLowerCase().contains("parking")) {
                truck = new MarkerOptions().position(car).title(position.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_blue));
            }

            // BitmapDescriptorFactory.fromResource(R.drawable.ic_truck_blue)
            mMap.addMarker(truck);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(car, 10.0f));
        }
    }

    private void addToMapNoRefresh() {
        for (RealtimePosition position : realtimePositions) {
            LatLng car = new LatLng(position.getDetail().getLatitude(), position.getDetail().getLongitude());
            MarkerOptions truck = new MarkerOptions().position(car).title(position.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_yellow));
            if (position.getRemark().toLowerCase().contains("driving")) {
                truck = new MarkerOptions().position(car).title(position.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_green));
            } else if (position.getRemark().toLowerCase().contains("parking")) {
                truck = new MarkerOptions().position(car).title(position.getLicenseNo()).icon(bitmapDescriptorFromVector(this, R.drawable.ic_truck_blue));
            }

            // BitmapDescriptorFactory.fromResource(R.drawable.ic_truck_blue)
            mMap.addMarker(truck);
        }
    }

    public void getVehicle(final boolean refresh) throws JSONException {
        if (realtimePositions.size() > 0) {
            Log.i(MapsActivity.class.getSimpleName(), "Refresh");
            realtimePositions = new ArrayList<>();
        }
        JSONArray plates = new JSONArray();

        for (Vehicle vehicle : vehicles) {
            plates.put(vehicle.getLicenseNo());
        }
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");
        String token = preferences.getString("token", "");
        JSONObject parameter = new JSONObject();
        parameter.put("token", token);
        parameter.put("items", plates);
        parameter.put("activity", JSONObject.NULL);

        RequestBody requestBody = RequestBody.create(parameter.toString(),JSON);
        Request request = new Request.Builder()
                .url("http://fms.aksiteknologi.com/index.php/Api/Position/recent")
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    JSONObject data = new JSONObject(responseBody.string());
                    JSONArray positionData = data.getJSONArray("data");

                    for (int i = 0; i < positionData.length(); i++) {
                        JSONObject item = positionData.getJSONObject(i);

                        RealtimePosition realtimePosition = new RealtimePosition();
                        realtimePosition.setLicenseNo(item.getString("plate_no"));
                        realtimePosition.setSpeedIndicator(String.valueOf(item.getInt("speed")));
                        realtimePosition.setAccIndicator((item.getInt("acc") == 0) ? "Off" : "On");
                        realtimePosition.setAddressPosition(item.getString("address"));
                        realtimePosition.setRemark(item.getString("status_remark") +" "+ item.getString("duration"));

                        RealtimePositionDetail realtimePositionDetail = new RealtimePositionDetail();
                        realtimePositionDetail.setLatitude(item.getDouble("latitude"));
                        realtimePositionDetail.setLongitude(item.getDouble("longitude"));

                        realtimePosition.setDetail(realtimePositionDetail);

                        realtimePositions.add(realtimePosition);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (refresh) {
                                addToMapNoRefresh();
                            } else {
                                addToMap();
                            }

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getVehicleGroup(final boolean refresh) throws JSONException {
        if (realtimePositions.size() > 0) {
            Log.i(MapsActivity.class.getSimpleName(), "Refresh");
            realtimePositions = new ArrayList<>();
        }
        JSONArray plates = new JSONArray();

        for (VehicleGroup vehicleGroup : vehicleGroups) {
            plates.put(vehicleGroup.getGroupCode());
        }
        final MediaType JSON
                = MediaType.get("application/json; charset=utf-8");
        String token = preferences.getString("token", "");
        JSONObject parameter = new JSONObject();
        parameter.put("token", token);
        parameter.put("categories", plates);
        parameter.put("activity", JSONObject.NULL);
        parameter.put("plate_no", JSONObject.NULL);

        Log.i(MapsActivity.class.getSimpleName(), parameter.toString());

        RequestBody requestBody = RequestBody.create(parameter.toString(),JSON);
        Request request = new Request.Builder()
                .url("http://fms.aksiteknologi.com/index.php/Api/Categories/detail")
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    JSONObject data = new JSONObject(responseBody.string());
                    JSONArray positionData = data.getJSONArray("data");

                    Log.i(MapsActivity.class.getSimpleName(), positionData.toString());

                    for (int i = 0; i < positionData.length(); i++) {
                        JSONObject item = positionData.getJSONObject(i);

                        RealtimePosition realtimePosition = new RealtimePosition();
                        realtimePosition.setLicenseNo(item.getString("plate_no"));
                        realtimePosition.setSpeedIndicator(String.valueOf(item.getInt("speed")));
                        realtimePosition.setAccIndicator((item.getInt("acc") == 0) ? "Off" : "On");
                        realtimePosition.setAddressPosition(item.getString("address"));
                        realtimePosition.setRemark(item.getString("status_remark") +" "+ item.getString("duration"));

                        RealtimePositionDetail realtimePositionDetail = new RealtimePositionDetail();
                        realtimePositionDetail.setLatitude(item.getDouble("latitude"));
                        realtimePositionDetail.setLongitude(item.getDouble("longitude"));

                        realtimePosition.setDetail(realtimePositionDetail);

                        realtimePositions.add(realtimePosition);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (refresh) {
                                addToMapNoRefresh();
                            } else {
                                addToMap();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        // Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        // vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        // vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
