package com.aksiteknologi.aksifms.activity.ui.reatlimemap;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.activity.MapsActivity;
import com.aksiteknologi.aksifms.adapter.CarGroupViewAdapter;
import com.aksiteknologi.aksifms.model.Vehicle;
import com.aksiteknologi.aksifms.model.VehicleGroup;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RealtimeCarGroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RealtimeCarGroupFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private ArrayList<VehicleGroup> vehicleGroups;

    public RealtimeCarGroupFragment() {
        // Required empty public constructor
    }

    public static RealtimeCarGroupFragment newInstance(ArrayList<VehicleGroup> vehicleGroups) {
        RealtimeCarGroupFragment fragment = new RealtimeCarGroupFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, vehicleGroups);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            vehicleGroups = getArguments().getParcelableArrayList(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_realtime_car_group, container, false);
        RecyclerView mainView = view.findViewById(R.id.listView);
        AppCompatButton mapButton = view.findViewById(R.id.showMap);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mainView.setLayoutManager(layoutManager);
        final CarGroupViewAdapter carGroupViewAdapter = new CarGroupViewAdapter(getContext(), this.vehicleGroups);
        mainView.setAdapter(carGroupViewAdapter);

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<VehicleGroup> vehicleGroups = carGroupViewAdapter.getVehicleGroups();
                ArrayList<VehicleGroup> selectedVehicleGroups = new ArrayList<>();

                for (VehicleGroup vehicleGroup : vehicleGroups) {
                    if (vehicleGroup.getSelected()) {
                        Log.i(RealtimeCarGroupFragment.class.getSimpleName(), vehicleGroup.getGroupCode());
                        selectedVehicleGroups.add(vehicleGroup);
                    }
                }
                Intent mapsView = new Intent(getContext(), MapsActivity.class);
                mapsView.putParcelableArrayListExtra("selected_group", selectedVehicleGroups);
                getContext().startActivity(mapsView);
            }
        });

        return view;
    }
}
