package com.aksiteknologi.aksifms.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.activity.RealtimePositionDetailActivity;
import com.aksiteknologi.aksifms.model.RealtimePosition;

import java.util.ArrayList;

public class RealtimePositionViewAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<RealtimePosition> realtimePositions;
    private int testCount;

    public RealtimePositionViewAdapter(Context context) {
        this.context = context;
    }

    public RealtimePositionViewAdapter(Context context, ArrayList<RealtimePosition> realtimePositions) {
        this.context = context;
        this.realtimePositions = realtimePositions;
    }

    //  View-holder
    public static class RealtimePositionViewHolder extends RecyclerView.ViewHolder {

        TextView licenseNo, cityLocation, speedIndicator, accIndicator, addressPosition, remark, updateAt;
        ImageView carIcon, remarkIcon;

        public RealtimePositionViewHolder(@NonNull View itemView) {
            super(itemView);

            licenseNo = itemView.findViewById(R.id.license_no);
            cityLocation = itemView.findViewById(R.id.city_location);
            speedIndicator = itemView.findViewById(R.id.speed_indicator);
            accIndicator = itemView.findViewById(R.id.acc_indicator);
            addressPosition = itemView.findViewById(R.id.address_position);
            remark = itemView.findViewById(R.id.item_remark);
            updateAt = itemView.findViewById(R.id.updated_at);
            carIcon = itemView.findViewById(R.id.icon);
            remarkIcon = itemView.findViewById(R.id.remark_icon);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.realtime_position_cell_item, parent, false);
        return new RealtimePositionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ((RealtimePositionViewHolder) holder).licenseNo.setText(realtimePositions.get(position).getLicenseNo());
        ((RealtimePositionViewHolder) holder).cityLocation.setText(realtimePositions.get(position).getCityLocation());
        ((RealtimePositionViewHolder) holder).speedIndicator.setText(realtimePositions.get(position).getSpeedIndicator()+" Km/h");
        ((RealtimePositionViewHolder) holder).accIndicator.setText("Acc: "+realtimePositions.get(position).getAccIndicator());
        ((RealtimePositionViewHolder) holder).addressPosition.setText(realtimePositions.get(position).getAddressPosition());
        ((RealtimePositionViewHolder) holder).remark.setText(realtimePositions.get(position).getRemark());
        ((RealtimePositionViewHolder) holder).updateAt.setText(realtimePositions.get(position).getGpsTime());

        ((RealtimePositionViewHolder) holder).carIcon.setImageDrawable(context.getDrawable(realtimePositions.get(position).getCarIcon()));
        ((RealtimePositionViewHolder) holder).remarkIcon.setImageDrawable(context.getDrawable(realtimePositions.get(position).getRemarkIcon()));

        ((RealtimePositionViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent realtimePositionDetail = new Intent(context, RealtimePositionDetailActivity.class);
                realtimePositionDetail.putExtra("detail_data", realtimePositions.get(position));
                context.startActivity(realtimePositionDetail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return realtimePositions.size();
    }
}
