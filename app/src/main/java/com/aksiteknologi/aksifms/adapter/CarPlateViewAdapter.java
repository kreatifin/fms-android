package com.aksiteknologi.aksifms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.model.Vehicle;

import java.util.ArrayList;

public class CarPlateViewAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<Vehicle> vehicles;

    public CarPlateViewAdapter(Context context, ArrayList<Vehicle> vehicles) {
        this.context = context;
        this.vehicles = vehicles;
    }

    public static class CarPlateViewHolder extends RecyclerView.ViewHolder {

        TextView plateNo;
        View checkList;
        public CarPlateViewHolder(@NonNull View itemView) {
            super(itemView);
            plateNo = itemView.findViewById(R.id.title);
            checkList = itemView.findViewById(R.id.checkList);
        }

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.relatime_car_plate_cell_item, parent, false);
        return new CarPlateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ((CarPlateViewHolder) holder).plateNo.setText(this.vehicles.get(position).getLicenseNo());
        if (!this.vehicles.get(position).getSelected()) {
            ((CarPlateViewHolder) holder).checkList.setVisibility(View.INVISIBLE);
        } else {
            ((CarPlateViewHolder) holder).checkList.setVisibility(View.VISIBLE);
        }
        ((CarPlateViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vehicles.get(position).getSelected()) {
                    vehicles.get(position).setSelected(false);
                } else {
                    vehicles.get(position).setSelected(true);
                }

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicles.size();
    }

    public ArrayList<Vehicle> getVehicles() {
        return this.vehicles;
    }
}
