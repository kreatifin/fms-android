package com.aksiteknologi.aksifms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.model.Vehicle;
import com.aksiteknologi.aksifms.model.VehicleGroup;

import java.util.ArrayList;

public class CarGroupViewAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<VehicleGroup> vehicleGroups;

    public CarGroupViewAdapter(Context context, ArrayList<VehicleGroup> vehicleGroups) {
        this.context = context;
        this.vehicleGroups = vehicleGroups;
    }

    public class CarGroupViewHolder extends RecyclerView.ViewHolder {

        TextView cityName;
        View checkList;
        public CarGroupViewHolder(@NonNull View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.title);
            checkList = itemView.findViewById(R.id.checkList);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.relatime_car_plate_cell_item, parent, false);
        return new CarGroupViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ((CarGroupViewHolder) holder).cityName.setText(vehicleGroups.get(position).getGroupCode());
        if (!this.vehicleGroups.get(position).getSelected()) {
            ((CarGroupViewHolder) holder).checkList.setVisibility(View.INVISIBLE);
        } else {
            ((CarGroupViewHolder) holder).checkList.setVisibility(View.VISIBLE);
        }
        ((CarGroupViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vehicleGroups.get(position).getSelected()) {
                    vehicleGroups.get(position).setSelected(false);
                } else {
                    vehicleGroups.get(position).setSelected(true);
                }

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicleGroups.size();
    }

    public ArrayList<VehicleGroup> getVehicleGroups() {
        return this.vehicleGroups;
    }
}
