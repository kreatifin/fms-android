package com.aksiteknologi.aksifms.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.activity.RealtimeMapActivity;
import com.aksiteknologi.aksifms.activity.RealtimePositionActivity;
import com.aksiteknologi.aksifms.model.MainMenu;

import java.util.ArrayList;

public class MainViewAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<MainMenu> mainMenus;

    public MainViewAdapter(Context context, ArrayList<MainMenu> mainMenus) {
        this.context = context;
        this.mainMenus = mainMenus;
    }

    //  View-holder
    public static class MainMenuViewHolder extends RecyclerView.ViewHolder {

        TextView menuItem;
        ImageView menuIcon;
        public MainMenuViewHolder(@NonNull View itemView) {
            super(itemView);

            menuItem = itemView.findViewById(R.id.menu_name);
            menuIcon = itemView.findViewById(R.id.menu_icon);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_menu_item, parent, false);
        return new MainMenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((MainMenuViewHolder) holder).menuItem.setText(mainMenus.get(position).getTitle());
        ((MainMenuViewHolder) holder).menuIcon.setImageDrawable(context.getDrawable(mainMenus.get(position).getIcon()));
        ((MainMenuViewHolder) holder).itemView.setTag(position);

        ((MainMenuViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((int) view.getTag() == 0) {
                    Intent realtimePosition = new Intent(context, RealtimePositionActivity.class);
                    context.startActivity(realtimePosition);
                } else if ((int) view.getTag() == 1) {
                    Intent realtimeMap = new Intent(context, RealtimeMapActivity.class);
                    context.startActivity(realtimeMap);
                } else {
                    Toast.makeText(context, "Under Development", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return mainMenus.size();
    }
}
