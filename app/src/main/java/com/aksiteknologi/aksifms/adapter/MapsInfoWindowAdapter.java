package com.aksiteknologi.aksifms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aksiteknologi.aksifms.R;
import com.aksiteknologi.aksifms.model.RealtimePosition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class MapsInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Context context;
    private RealtimePosition realtimePosition;
    private View window;

    public MapsInfoWindowAdapter(Context context, RealtimePosition realtimePosition) {
        this.context = context;
        this.realtimePosition = realtimePosition;
        window = LayoutInflater.from(context).inflate(R.layout.info_window_view, null);
    }

    private void renderView(RealtimePosition realtimePosition, View view) {
        TextView licenseNo, speedIndicator, accIndicator, addressPosition, remark, updateAt;
        ImageView remarkIcon;

        licenseNo = view.findViewById(R.id.license_no);
        speedIndicator = view.findViewById(R.id.speed_indicator);
        accIndicator = view.findViewById(R.id.acc_indicator);
        addressPosition = view.findViewById(R.id.address_position);
        remark = view.findViewById(R.id.item_remark);
        remarkIcon = view.findViewById(R.id.remark_icon);
        updateAt = view.findViewById(R.id.updated_at);

        licenseNo.setText(realtimePosition.getLicenseNo());
        speedIndicator.setText(realtimePosition.getSpeedIndicator()+" Km/h");
        accIndicator.setText("Acc: "+realtimePosition.getAccIndicator());
        addressPosition.setText(realtimePosition.getAddressPosition());
        remark.setText(realtimePosition.getRemark());
        updateAt.setText(realtimePosition.getGpsTime());
        remarkIcon.setImageDrawable(context.getDrawable(realtimePosition.getRemarkIcon()));
    }

    @Override
    public View getInfoWindow(Marker marker) {
        renderView(realtimePosition, window);
        return window;
    }

    @Override
    public View getInfoContents(Marker marker) {
        renderView(realtimePosition, window);
        return window;
    }
}
