package com.aksiteknologi.aksifms.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Vehicle implements Parcelable {
    private String id;
    private String licenseNo;
    private String stnkNo;
    private String kirNo;
    private String stnkExp;
    private String kirExp;
    private Boolean selected;

    public Vehicle(String id, String licenseNo, String stnkNo, String kirNo, String stnkExp, String kirExp, Boolean selected) {
        this.id = id;
        this.licenseNo = licenseNo;
        this.stnkNo = stnkNo;
        this.kirNo = kirNo;
        this.stnkExp = stnkExp;
        this.kirExp = kirExp;
        this.selected = selected;
    }

    protected Vehicle(Parcel in) {
        id = in.readString();
        licenseNo = in.readString();
        stnkNo = in.readString();
        kirNo = in.readString();
        stnkExp = in.readString();
        kirExp = in.readString();
        byte tmpSelected = in.readByte();
        selected = tmpSelected == 0 ? null : tmpSelected == 1;
    }

    public static final Creator<Vehicle> CREATOR = new Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getStnkNo() {
        return stnkNo;
    }

    public void setStnkNo(String stnkNo) {
        this.stnkNo = stnkNo;
    }

    public String getKirNo() {
        return kirNo;
    }

    public void setKirNo(String kirNo) {
        this.kirNo = kirNo;
    }

    public String getStnkExp() {
        return stnkExp;
    }

    public void setStnkExp(String stnkExp) {
        this.stnkExp = stnkExp;
    }

    public String getKirExp() {
        return kirExp;
    }

    public void setKirExp(String kirExp) {
        this.kirExp = kirExp;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(licenseNo);
        parcel.writeString(stnkNo);
        parcel.writeString(kirNo);
        parcel.writeString(stnkExp);
        parcel.writeString(kirExp);
        parcel.writeByte((byte) (selected == null ? 0 : selected ? 1 : 2));
    }
}
