package com.aksiteknologi.aksifms.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class RealtimePosition implements Serializable {
    String licenseNo;
    String cityLocation;
    String speedIndicator;
    String accIndicator;
    String addressPosition;
    String remark;
    String gpsTime;
    RealtimePositionDetail detail;
    int carIcon;
    int remarkIcon;

    public RealtimePosition(String licenseNo, String cityLocation, String speedIndicator, String accIndicator, String addressPosition, String remark, String gpsTime, RealtimePositionDetail detail, int carIcon, int remarkIcon) {
        this.licenseNo = licenseNo;
        this.cityLocation = cityLocation;
        this.speedIndicator = speedIndicator;
        this.accIndicator = accIndicator;
        this.addressPosition = addressPosition;
        this.remark = remark;
        this.gpsTime = gpsTime;
        this.detail = detail;
        this.carIcon = carIcon;
        this.remarkIcon = remarkIcon;
    }

    public RealtimePosition() {
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getCityLocation() {
        return cityLocation;
    }

    public void setCityLocation(String cityLocation) {
        this.cityLocation = cityLocation;
    }

    public String getSpeedIndicator() {
        return speedIndicator;
    }

    public void setSpeedIndicator(String speedIndicator) {
        this.speedIndicator = speedIndicator;
    }

    public String getAccIndicator() {
        return accIndicator;
    }

    public void setAccIndicator(String accIndicator) {
        this.accIndicator = accIndicator;
    }

    public String getAddressPosition() {
        return addressPosition;
    }

    public void setAddressPosition(String addressPosition) {
        this.addressPosition = addressPosition;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getGpsTime() {
        return gpsTime;
    }

    public void setGpsTime(String gpsTime) {
        this.gpsTime = gpsTime;
    }

    public RealtimePositionDetail getDetail() {
        return detail;
    }

    public void setDetail(RealtimePositionDetail detail) {
        this.detail = detail;
    }

    public int getCarIcon() {
        return carIcon;
    }

    public void setCarIcon(int carIcon) {
        this.carIcon = carIcon;
    }

    public int getRemarkIcon() {
        return remarkIcon;
    }

    public void setRemarkIcon(int remarkIcon) {
        this.remarkIcon = remarkIcon;
    }
}
