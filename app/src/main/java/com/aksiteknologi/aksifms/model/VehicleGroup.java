package com.aksiteknologi.aksifms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VehicleGroup implements Parcelable {
    private String groupCode;
    private Boolean selected;

    public VehicleGroup(String groupCode, Boolean selected) {
        this.groupCode = groupCode;
        this.selected = selected;
    }

    public VehicleGroup() {
    }

    protected VehicleGroup(Parcel in) {
        groupCode = in.readString();
        byte tmpSelected = in.readByte();
        selected = tmpSelected == 0 ? null : tmpSelected == 1;
    }

    public static final Creator<VehicleGroup> CREATOR = new Creator<VehicleGroup>() {
        @Override
        public VehicleGroup createFromParcel(Parcel in) {
            return new VehicleGroup(in);
        }

        @Override
        public VehicleGroup[] newArray(int size) {
            return new VehicleGroup[size];
        }
    };

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(groupCode);
        parcel.writeByte((byte) (selected == null ? 0 : selected ? 1 : 2));
    }
}
