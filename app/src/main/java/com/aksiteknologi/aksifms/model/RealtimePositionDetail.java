package com.aksiteknologi.aksifms.model;

import java.io.Serializable;

public class RealtimePositionDetail implements Serializable {
    Double odometer;
    Double latitude, longitude;
    Integer gpsSignal;
    String driver, reportStatus;
    Integer tempOne, tempTwo;

    public RealtimePositionDetail(Double odometer, Double latitude, Double longitude, Integer gpsSignal, String driver, String reportStatus, Integer tempOne, Integer tempTwo) {
        this.odometer = odometer;
        this.latitude = latitude;
        this.longitude = longitude;
        this.gpsSignal = gpsSignal;
        this.driver = driver;
        this.reportStatus = reportStatus;
        this.tempOne = tempOne;
        this.tempTwo = tempTwo;
    }

    public RealtimePositionDetail() {
    }

    public Double getOdometer() {
        return odometer;
    }

    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getGpsSignal() {
        return gpsSignal;
    }

    public void setGpsSignal(Integer gpsSignal) {
        this.gpsSignal = gpsSignal;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(String reportStatus) {
        this.reportStatus = reportStatus;
    }

    public Integer getTempOne() {
        return tempOne;
    }

    public void setTempOne(Integer tempOne) {
        this.tempOne = tempOne;
    }

    public Integer getTempTwo() {
        return tempTwo;
    }

    public void setTempTwo(Integer tempTwo) {
        this.tempTwo = tempTwo;
    }
}
