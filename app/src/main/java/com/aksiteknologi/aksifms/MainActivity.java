package com.aksiteknologi.aksifms;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.aksiteknologi.aksifms.adapter.MainViewAdapter;
import com.aksiteknologi.aksifms.adapter.decoration.MainViewItemDecoration;
import com.aksiteknologi.aksifms.model.MainMenu;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mainView;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getApplicationContext().getSharedPreferences(getString(R.string.app_pref_key), MODE_PRIVATE);
        initView();
    }

    private void initView() {
        mainView = findViewById(R.id.main_view);

        ArrayList<MainMenu> mainMenus = new ArrayList<>();
        MainMenu realtimePosition = new MainMenu("Realtime Position", R.drawable.ic_car_white_32);
        MainMenu realtimeMap = new MainMenu("Realtime by Map", R.drawable.ic_map);

        mainMenus.add(realtimePosition);
        mainMenus.add(realtimeMap);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);

        mainView.setHasFixedSize(true);
        mainView.setLayoutManager(gridLayoutManager);

        MainViewAdapter adapter = new MainViewAdapter(this, mainMenus);
        mainView.setAdapter(adapter);
        mainView.addItemDecoration(new MainViewItemDecoration(8));
    }

    private void logout() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("token");
        editor.apply();
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
